﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AppleZone.Web.Startup))]
namespace AppleZone.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
